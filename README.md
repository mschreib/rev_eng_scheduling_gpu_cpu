
Test scheduling of GPU and/or CPU

Run

	$ ./run_test_opencl.sh

which will generate an output image which grayscale value indicates the 
execution order.

Darker means started earlier, brighter means started later.


There are various program options available, see -h
	$ ./run_test_opencl.sh -h

including specifying the workgroup sizes with -l and -m.




For Ubuntu, you can install the opencl headers by calling

	$ sudo apt install opencl-clhpp-headers ocl-icd-opencl-dev

