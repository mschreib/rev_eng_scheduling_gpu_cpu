
CXX = g++ 
LDFLAGS = -L/lib/x86_64-linux-gnu/
LDFLAGS += -lOpenCL
LDFLAGS += -lm

CSSFLAGS = 
CXXFLAGS += -Wall

DEPS=$(wildcard src/*.hpp) Makefile

all: test_opencl

test_opencl: build/main_opencl.o
	mkdir -p build
	$(CXX) $< $(LDFLAGS) -o build/test_opencl

build/main_opencl.o: src/main_opencl.cpp $(DEPS)
	mkdir -p build
	$(CXX) -c $(CXXFLAGS) $< -o $@

# cleaning (remove executables and what not)
clean:
	$(RM) -r ./build
