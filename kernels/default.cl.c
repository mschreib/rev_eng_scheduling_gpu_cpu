#pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : enable

__kernel
void test_kernel(
		__global uint *A,
		__global uint *counter
)
{
	int li0 = get_local_id(0);
	int li1 = get_local_id(1);

	int i0 = get_global_id(0);
	int i1 = get_global_id(1);

	int N0 = get_global_size(0);
	int N1 = get_global_size(1);

	// Skip all, but the first element of the local working group
	if (li0 != 0 || li1 != 0)
		return;

	// get counter
	int c = atomic_inc(counter);

	// update local counter
	A[i0*N1+i1] += c;
};
