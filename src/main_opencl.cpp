/*
 * main.cpp
 *
 *  Created on: Feb 9, 2023
 *      Author: Martin Schreiber
 */

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

#include "Config.hpp"
#include "Array2D.hpp"
#include "Tools.hpp"
#include "Boilerplate.hpp"


#define CL_HPP_TARGET_OPENCL_VERSION 300
#define CL_HPP_ENABLE_EXCEPTIONS
#include <CL/opencl.hpp>


typedef cl_uint TYPE_DATA;


template <typename T>
void run_opencl_benchmark(
	Config &config,
	Array2D<T> &io_host_A,
	std::string &io_device_info	// should return some information about hardware
)
{
	/*
	 * Show and load platform
	 */
	std::vector<cl::Platform> platforms;
	cl::Platform::get(&platforms);

	std::cout << std::endl;
	for (int i = 0; i < (int)platforms.size(); i++)
	{
		auto &p = platforms[i];

		std::cerr << "+ Platform " << i << ":" << std::endl;
		std::cerr << "  + Name: " << p.getInfo<CL_PLATFORM_NAME>() << "\n";
		std::cerr << "  + Version: " << p.getInfo<CL_PLATFORM_VERSION>() << "\n";
		std::cerr << "  + Vendor: " << p.getInfo<CL_PLATFORM_VENDOR>() << "\n";
	}

	cl::Platform platform;

	if (config.platform_id >= 0)
	{
		if (config.platform_id >= (int)platforms.size())
		{
			std::cerr << "Invalid platform ID!" << std::endl;
			exit(EXIT_FAILURE);
		}

		platform = platforms[config.platform_id];
	}
	else
	{
		platform = cl::Platform::getDefault();
	}


	/*
	 * Load device
	 */
	std::vector<cl::Device> devices;
	platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);

	std::cout << std::endl;
	for (int i = 0; i < (int)devices.size(); i++)
	{
		auto &d = devices[i];

		std::cerr << "+ Device " << i << ":" << std::endl;
		std::cerr << "  + Name: " << d.getInfo<CL_DEVICE_NAME>() << "\n";
		std::cerr << "  + Version: " << d.getInfo<CL_DEVICE_VERSION>() << "\n";
	}


	cl::Device device;

	if (config.device_id >= 0)
	{
		if (config.device_id >= (int)devices.size())
		{
			std::cerr << "Invalid device ID!" << std::endl;
			exit(EXIT_FAILURE);
		}

		device = devices[config.device_id];
	}
	else
	{
		device = cl::Device::getDefault();
	}


	/*
	 * Generate context
	 */
	cl::Context context(device);
	cl::CommandQueue queue(context, device);

	/*
	 * We get additional information to create output
	 * files specific to this hardware we're benchmarking
	 */
	std::string device_info = platform.getInfo<CL_PLATFORM_VENDOR>() + " " + device.getInfo<CL_DEVICE_NAME>();
	device_info = str_replace(device_info, " ", "_");
	device_info = str_replace(device_info, "(", "_");
	device_info = str_replace(device_info, ")", "_");
	device_info = str_replace(device_info, "[", "_");
	device_info = str_replace(device_info, "]", "_");

	io_device_info += "_" + device_info;

	/*
	 * Setup buffers
	 */
	cl::Buffer buffer_A(context, CL_MEM_READ_WRITE, io_host_A.size());
	queue.enqueueWriteBuffer(buffer_A, CL_TRUE, 0, io_host_A.size(), io_host_A.data());

	cl::Buffer buffer_atomic_counter(context, CL_MEM_READ_WRITE, sizeof(cl_int));
	int atomic_counter_host = 0;
	// will be set to 0 in each iteration


	/*
	 * Load program, compile and create kernel
	 */

	std::string kernel_source;
	{
		std::ifstream t(config.kernel_source_file);
		std::stringstream buffer;
		buffer << t.rdbuf();

		kernel_source = buffer.str();
	}

	cl::Program::Sources programStrings;
	programStrings.push_back(kernel_source);

	cl::Program program(context, programStrings);

	try {
		// program.build("-cl-std=CL2.0");
		program.build(device);
	}
	catch (...) {
		cl_int buildErr = CL_SUCCESS;
		auto buildInfo = program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(&buildErr);
		for (auto &pair : buildInfo) {
			std::cerr << pair.second << std::endl;
		}
		exit(EXIT_FAILURE);
	}

	cl::Kernel kernel(program, "test_kernel");
	kernel.setArg(0, buffer_A);
	kernel.setArg(1, buffer_atomic_counter);

	/*
	 * Benchmarking queue
	 */
	cl::NDRange NDRange(config.N[0], config.N[1]);
	cl::NDRange lNDRange(config.lN[0], config.lN[1]);

	if (config.lN[0] <= 0 || config.lN[1] <= 0)
	{
		std::cout << "Letting the queue choose the optimal local workgroup size" << std::endl;
		lNDRange = cl::NullRange;
	}

	std::cout << "Running iterations:" << std::endl;
	for (int k = 0; k < config.num_iters_loop; k++)
	{
		std::cout << " + iteration " << k << " / " << config.num_iters_loop << "\n";

		// reset counter
		queue.enqueueWriteBuffer(buffer_atomic_counter, CL_TRUE, 0, sizeof(atomic_counter_host), &atomic_counter_host);

		// run kernel
		queue.enqueueNDRangeKernel(kernel, cl::NullRange, NDRange, lNDRange);
	}
	std::cout << std::endl;

	/*
	 * Get raw benchmark data back
	 */
	queue.enqueueReadBuffer(buffer_A, CL_TRUE, 0, io_host_A.size(), io_host_A.data());
}


int main(int argc, char *argv[])
{
	Boilerplate<TYPE_DATA> bp(argc, argv);

	run_opencl_benchmark(bp.config, bp.A, bp.par_model_and_device_info);

	bp.postprocessing();

	return 0;
}
