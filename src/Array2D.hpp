/*
 * Array2D.hpp
 *
 *  Created on: Feb 11, 2023
 *      Author: Martin Schreiber
 */

#ifndef SRC_RT_ARRAY2D_HPP_
#define SRC_RT_ARRAY2D_HPP_

#include <string>
#include <vector>
#include <array>


/**
 * Class for some data array which can be used also within
 * other parallelization models
 */
template <typename T>
class Array2D
{
public:
	std::array<size_t,2> N;
	int num_elements;

	std::vector<T> _data;

	void setup(size_t i_N0, size_t i_N1)
	{
		N[0] = i_N0;
		N[1] = i_N1;

		num_elements = N[0]*N[1];

		_data.resize(num_elements);

		set_zero();
	}


	void setup(const size_t *i_N)
	{
		setup(i_N[0], i_N[1]);
	}

	size_t size()
	{
		return sizeof(T)*num_elements;
	}

	T *data()
	{
		return _data.data();
	}

	Array2D(size_t i_N0, size_t i_N1)
	{
		setup(i_N0, i_N1);
	}

	Array2D(const size_t *i_N)
	{
		setup(i_N);
	}


	Array2D()	:
		num_elements(0)
	{
		N[0] = -1;
		N[1] = -1;
	}

	const T& get(int i0, int i1)	const
	{
		return _data[i0*N[1] + i1];
	}

	T& getVariable(int i0, int i1)
	{
		return _data[i0*N[1] + i1];
	}

	T min()	const
	{
		T min_val = (1 << (sizeof(T)*8-2));

		for (int i = 0; i < num_elements; i++)
		{
			if (min_val > _data[i])
				min_val = _data[i];
		}

		return min_val;
	}

	T max()	const
	{
		T max_val = 0;

		for (int i = 0; i < num_elements; i++)
		{
			if (max_val < _data[i])
				max_val = _data[i];
		}

		return max_val;
	}

	void set_zero()
	{
		for (int i = 0; i < num_elements; i++)
		{
			_data[i] = 0;
		}
	}


	void write_image(
			const std::string &i_output_file
	)
	{
		std::ofstream ofile(i_output_file);

		ofile << "P2" << std::endl;
		ofile << (int)N[1] << " " << (int)N[0] << std::endl;

		int max_num = 256*256-1;
		ofile << max_num << std::endl;

		T min_val = min();
		T max_val = max();

		T diff = max_val - min_val;
		for (std::size_t i0 = 0; i0 < N[0]; i0++)
		{
			for (std::size_t i1 = 0; i1 < N[1]; i1++)
			{
				ofile << (int)(max_num*(double)(get(i0, i1) - min_val)/(double)diff) << " ";
			}
			ofile << std::endl;
		}
	}


	bool autodetect_local_workgroup_size(std::size_t *o_new_N)
	{
		std::size_t i0;
		for (i0 = 1; i0 < N[0]; i0++)
		{
			if (get(i0, 0) != 0)
			{
				o_new_N[0] = i0;
				break;
			}
		}

		if (i0 == N[0])
			return false;

		std::size_t i1;
		for (i1 = 1; i1 < N[1]; i1++)
		{
			if (get(0, i1) != 0)
			{
				o_new_N[1] = i1;
				break;
			}
		}

		if (i1 == N[1])
			return false;

		return true;
	}


	Array2D<T> reduce_image(
			std::size_t *i_rN
	)
	{
		Array2D<T> ret_A(i_rN);

		// size of one block (should be local workgroup size)
		std::size_t L[2];
		L[0] = N[0]/ret_A.N[0];
		L[1] = N[1]/ret_A.N[1];

		if (N[0] % ret_A.N[0] != 0)
		{
			printf("ERROR");
			exit(1);
		}

		if (N[1] % ret_A.N[1] != 0)
		{
			printf("ERROR");
			exit(1);
		}

		// iterate over pixels of output image
		for (std::size_t r0 = 0; r0 < ret_A.N[0]; r0++)
		{
			for (std::size_t r1 = 0; r1 < ret_A.N[1]; r1++)
			{
				// set output to 0
				ret_A.getVariable(r0, r1) = 0;

				// iterate over local work items
				for (std::size_t l0 = 0; l0 < L[0]; l0++)
				{
					std::size_t i0 = r0*L[0] + l0;

					if (i0 >= N[0])
						break;

					// generate global work item index
					for (std::size_t l1 = 0; l1 < L[1]; l1++)
					{
						std::size_t i1 = r1*L[1] + l1;

						if (i1 >= N[1])
							break;

						ret_A.getVariable(r0, r1) += get(i0, i1);
					}
				}
			}
		}

		return ret_A;
	}
};


std::string str_replace(
		const std::string &i_string,
		const std::string &i_src,
		const std::string &i_dst
)
{
	int len_src = i_src.length();
	int len_dst = i_dst.length();

	std::string ret_string = i_string;

	std::size_t i = 0;
	while (true)
	{
	     i = ret_string.find(i_src, i);
	     if (i == std::string::npos) break;

	     ret_string = ret_string.substr(0, i)+i_dst+ret_string.substr(i+len_src);
	     i += len_dst;
	}

	return ret_string;
}

#endif
