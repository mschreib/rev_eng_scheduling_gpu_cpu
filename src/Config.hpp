/*
 * Data.hpp
 *
 *  Created on: Feb 11, 2023
 *      Author: Martin Schreiber
 */

#ifndef SRC_RE_CONFIG_HPP_
#define SRC_RE_CONFIG_HPP_

#include <string>
#include <iostream>

#include <unistd.h>
#include <stdlib.h>


/**
 * We use solely OpenCL terminology to describe the variables here
 */
class Config
{
public:
	/*
	 * Size of array used for benchmark
	 *
	 * Needs to be sufficiently large enough in combination with the local work group
	 */
	size_t N[2];

	/*
	 * Number of iterations to apply the kernel
	 */
	int num_iters_loop = 16;

	/*
	 * (OpenCL)
	 * local workgroup size
	 * Set to (0, 0) for autodetection
	 */
	size_t lN[2];

	/*
	 * Workgroup size of reduced array
	 *
	 * This is the array which we'll use for displaying results
	 */
	size_t rN[2];

	/*
	 * (OpenCL) Platform & device information
	 *
	 * For OpenCL:
	 *  * Set to -1 for autodetection
	 */
	int platform_id = -1;
	int device_id = -1;

	/*
	 * (OpenCL) kernel source file to use
	 */
	std::string kernel_source_file = "kernels/default.cl.c";


	/*
	 * Output file for the entire data
	 */
	std::string output_image_file_all = "output_%s_all.ppm";

	/*
	 * Output file for the reduced data
	 */
	std::string output_image_file_reduced = "output_%s_reduced.ppm";

	Config()
	{
		N[0] = 1024;
		N[1] = 2048;

		lN[0] = 0;
		lN[1] = 0;
	}

	void setup_reduced_array(size_t *i_lN)
	{
		lN[0] = i_lN[0];
		lN[1] = i_lN[1];

	    if (lN[0] > 0)
	    {
			if (N[0] % lN[0]) {
				std::cout << "N[0] not evenly dividable by lN[0]" << std::endl;
				exit(1);
			}
	    }

	    if (lN[1] > 0)
	    {
			if (N[1] % lN[1]) {
				std::cout << "N[1] not evenly dividable by lN[1]" << std::endl;
				exit(1);
			}
	    }

	    rN[0] = N[0] / lN[0];
	    rN[1] = N[1] / lN[1];
	}

	void parse_parameters(int argc, char *const argv[])
	{

		int opt;
	    while ((opt = getopt(argc, argv, "x:y:i:l:m:p:d:k:a:o:")) != -1) {
	        switch (opt) {
	        case 'x':	N[0] = atoi(optarg);	break;
	        case 'y':	N[1] = atoi(optarg);	break;
	        case 'i':	num_iters_loop = atoi(optarg);	break;
	        case 'l':	lN[0] = atoi(optarg);	break;
	        case 'm':	lN[1] = atoi(optarg);	break;
	        case 'p':	platform_id = atoi(optarg);	break;
	        case 'd':	device_id = atoi(optarg);	break;
	        case 'k':	kernel_source_file = optarg;	break;
	        case 'a':	output_image_file_all = optarg;	break;
	        case 'o':	output_image_file_reduced = optarg;	break;
	        default:
	        	print_usage(argc, argv);
	            exit(EXIT_FAILURE);
	        }
	    }
	}

	void print_usage(int argc, char *const argv[])
	{
		std::cout << std::endl;
		std::cout << "Usage:" << std::endl;
		std::cout << std::endl;
		std::cout << "	" << argv[0];
		std::cout << " -x [Nx=" << (int)N[0] << "]";
		std::cout << " -y [Ny=" << (int)N[1] << "]";
		std::cout << " -i [num_iters_loop=" << num_iters_loop << "]";
		std::cout << " -p [platform_id=" << platform_id << "]";
		std::cout << " -d [device_id=" << device_id << "]";
		std::cout << " -l [Lx=" << (int)lN[0] << "]";
		std::cout << " -m [Ly=" << (int)lN[1] << "]";
		std::cout << " -k [kernel=" << kernel_source_file << "]";
		std::cout << " -a [outptu_image_file_all=" << output_image_file_all << "]";
		std::cout << " -o [outptu_image_file_reduced=" << output_image_file_all << "]";
		std::cout << std::endl;
		std::cout << std::endl;
		exit(1);
	}

	void print()
	{
		std::cout << " + N[0]: " << N[0] << std::endl;
		std::cout << " + N[1]: " << N[1] << std::endl;
		std::cout << " + num_iters: " << num_iters_loop << std::endl;
		std::cout << " + platform_id: " << platform_id << std::endl;
		std::cout << " + device_id: " << device_id << std::endl;
		std::cout << " + lN[0]: " << lN[0] << std::endl;
		std::cout << " + lN[1]: " << lN[1] << std::endl;
		std::cout << " + kernel_source_file: " << kernel_source_file << std::endl;
		std::cout << " + output_image_file_all: " << output_image_file_all << std::endl;
		std::cout << " + output_image_file_reduced: " << output_image_file_reduced << std::endl;
	}
};



#endif
