/*
 * Boilerplate.hpp
 *
 *  Created on: Feb 11, 2023
 *      Author: Martin Schreiber
 */

#ifndef SRC_BOILERPLATE_HPP_
#define SRC_BOILERPLATE_HPP_

#include "Config.hpp"
#include "Array2D.hpp"
#include "Tools.hpp"

template <typename T>
class Boilerplate
{
public:
	Config config;

	std::string par_model_and_device_info = "opencl";

	Array2D<T> A;


	Boilerplate(int argc, char *argv[])
	{
		config.parse_parameters(argc, argv);
		config.print();

		A.setup(config.N);
	}

	void postprocessing()
	{
		if (config.output_image_file_all != "-")
		{
			char buffer[config.output_image_file_all.length()+par_model_and_device_info.length()+2];
			sprintf(buffer, config.output_image_file_all.c_str(), par_model_and_device_info.c_str());

			std::string filename = buffer;
			filename = str_replace(filename, "__", "_");

			std::cout << "Writing image with all array values: " << filename << std::endl;
			A.write_image(filename);
		}
		std::cout << std::endl;

		if (config.output_image_file_reduced != "-")
		{
			if (config.lN[0] <= 0)
			{
				std::cout << "Autodetecting local workgroup size" << std::endl;

				/*
				 * We try to autodetect the local workgroup size
				 *
				 * WARNING: This only works with a particular kernel (e.g., the standard one)
				 */

				std::size_t new_lN[2];
				bool retval = A.autodetect_local_workgroup_size(new_lN);

				if (!retval)
				{
					std::cerr << "ERROR: Failed to autodetect local workgroup size" << std::endl;
					exit(EXIT_FAILURE);
				}

				config.setup_reduced_array(new_lN);

				std::cout << "Detected local group size of (" << new_lN[0] << ", " << new_lN[1] << ")" << std::endl;
			}
			else
			{
				config.setup_reduced_array(config.lN);
			}

			Array2D<T> A_reduced_host = A.reduce_image(config.rN);

			{
				char buffer[config.output_image_file_reduced.length()+par_model_and_device_info.length()+2];
				sprintf(buffer, config.output_image_file_reduced.c_str(), par_model_and_device_info.c_str());

				std::string filename = buffer;
				filename = str_replace(filename, "__", "_");

				std::cout << "Writing image with reduced array values: " << filename << std::endl;
				A_reduced_host.write_image(filename);
			}
		}
		std::cout << std::endl;

		std::cout << "FIN" << std::endl;
	}
};


#endif /* SRC_BOILERPLATE_HPP_ */
