/*
 * Tools.hpp
 *
 *  Created on: Feb 11, 2023
 *      Author: Martin Schreiber
 */

#ifndef SRC_RE_TOOLS_HPP_
#define SRC_RE_TOOLS_HPP_

#include <string>

class Tools
{
	static
	std::string str_replace(
			const std::string &i_string,
			const std::string &i_src,
			const std::string &i_dst
	)
	{
		int len_src = i_src.length();
		int len_dst = i_dst.length();

		std::string ret_string = i_string;

		std::size_t i = 0;
		while (true)
		{
			 i = ret_string.find(i_src, i);
			 if (i == std::string::npos) break;

			 ret_string = ret_string.substr(0, i)+i_dst+ret_string.substr(i+len_src);
			 i += len_dst;
		}

		return ret_string;
	}
};




#endif /* SRC_TOOLS_HPP_ */
