#! /bin/bash


# stop on first error
set -e


make clean
make test_opencl

N0=$((2**11))
N1=$((2**12))

E="./build/test_opencl -x $N0 -y $N1 -i 100 -l 16 -m 16 -a - $@"
echo "$E"
$E

